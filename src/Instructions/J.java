package Instructions;

public class J extends Instruction{

	@Override 
 	public void setInstrBinaire() {
		
		int[] myInstrBinaire = new int[32];
		int[] myValue = new int[25];
		int[] myOpcode = new int[6];
		
		myValue = getValue();
		myOpcode = getOpcode();		
		
	 for(int i = 0; i < 6 ; i++ ) myInstrBinaire[i] = myOpcode[i];
	 for(int i = 6; i < 32 ; i ++) myInstrBinaire[i] = myValue[(31-i)];		 
	 
	 InstrBinaire = myInstrBinaire;	 
	 

}


}

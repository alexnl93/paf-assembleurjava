package Instructions;

public class I extends Instruction {
	
	@Override 
 	public void setInstrBinaire() {
		
		int[] myInstrBinaire = new int[32];
		int[] myRs1 = new int[5] ;
		int[] myImmediate = new int[16];
		int[] myRd = new int[5];
		int[] myOpcode = new int[6];
		
		myRs1 = getRs1();
		myImmediate = getImmediate();
		myRd = getRd();
		myOpcode = getOpcode();		
		
		
	 for(int i = 0; i < 6 ; i++ ) myInstrBinaire[i] = myOpcode[i];
	 for(int i = 6; i < 11; i ++) myInstrBinaire[i] = myRs1[4-(10-i)];
	 for(int i = 11; i < 16; i ++) myInstrBinaire[i] = myRd[(15-i)];
	 for(int i = 16; i < 32; i ++) myInstrBinaire[i] = myImmediate[15-(31-i)];
	 
	 InstrBinaire = myInstrBinaire;

}


}

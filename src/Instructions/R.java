package Instructions;

public class R extends Instruction {

	@Override 
	 	public void setInstrBinaire() {
		
		int[] myInstrBinaire = new int[32];
		int[] myRs1 = new int[5] ;
		int[] myRs2 = new int[5];
		int[] myRd = new int[5];
		int[] myOpcode = new int[6];
		
		myRs1 = getRs1();
		myRs2 = getRs2();
		myRd = getRd();
		myOpcode = getOpcode();
		
		 for(int i = 0; i < 6 ; i++ ) myInstrBinaire[i] = 0;
		 for(int i = 6; i < 11 ; i ++) myInstrBinaire[i] = myRs1[(10-i)];
		 for(int i = 11; i < 16 ; i ++) myInstrBinaire[i] = myRs2[4-(15-i)];
		 for(int i = 16; i < 21 ; i ++) myInstrBinaire[i] = myRd[(20-i)];		 
		 for(int i = 21; i < 26 ; i++ ) myInstrBinaire[i] = 0;
		 for(int i = 26; i < 32 ; i ++) myInstrBinaire[i] = myOpcode[5-(31-i)];

		 InstrBinaire = myInstrBinaire;
		 
	}
	
}

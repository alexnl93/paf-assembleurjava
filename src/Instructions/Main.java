package Instructions;

import java.io.*; 
import java.util.StringTokenizer;

public class Main {
	
	  public static  String ligne = "";
	  
	  public static void main(String[] argv) throws IOException
	  {
		  BufferedReader lecteurFichierEntre = null; 
		  ListeInstruction myListeInstruction = new ListeInstruction();
	
		  try { 
			  lecteurFichierEntre = new BufferedReader(new FileReader(new File(argv[0])));
		/*	  if (lecteurFichierEntre == null) { 
				  throw new FileNotFoundException("Fichier non trouv�: " 
				  + argv[0]); } */
		  } catch (FileNotFoundException e) { 

			  System.out.println("Fichier non trouv�: "  + "consignes.txt"); 
			  
		  } finally {
			  
			  int compteur = 0;
			  
			  while ((ligne = lecteurFichierEntre.readLine())!=null)
			  
	   { 
		   
		   compteur += 1;
		   
		   System.out.println("Instruction ligne num�ro :" + compteur);
		   
		   Instruction abc = new Instruction();
		   String myInst = "";
		   String resteLigne = "";
		   int myType = 0 , myRs1 = 0, myRs2 = 0 , myRd = 0, myImmediate = 0, myValue = 0;
		   
		   String rs1Tampon = "";
		   String rs2Tampon = "";
		   String rdTampon = "";	
		   String immediateTampon = "";	
		   String valueTampon = "";
		   
//	       System.out.println("rs1Tampon" + rs1Tampon);
//		   System.out.println("myRs1" + myRs1);
//		   ligne = lecteurFichierEntre.readLine();
//		   myListeInstruction.addInstruction(a);
		   System.out.println(ligne);
		   
		   StringTokenizer st1 = new StringTokenizer(ligne);
		   myInst = st1.nextToken();
		   
		   resteLigne = st1.nextToken();
		   StringTokenizer st2 = new StringTokenizer(resteLigne,",");
		   
		   abc.setInst(myInst);
		   abc.tri();
		   myType = abc.getType();
		   System.out.println("Le type est" + myType);
		   System.out.println("L'Opcode est " + abc.getOpcode().toString());
		   
		   switch(myType) {
		   
	
		   
		   case 0:
			   R a = new R(); 
			   
			   rdTampon = st2.nextToken();
			   rs1Tampon = st2.nextToken();
			   rs2Tampon = st2.nextToken();
			   
			   rs1Tampon = rs1Tampon.substring(1, rs1Tampon.length());
			   rs2Tampon = rs2Tampon.substring(1, rs2Tampon.length());
			   rdTampon = rdTampon.substring(1, rdTampon.length());
			   
			   myRs1 = Integer.valueOf(rs1Tampon);
			   myRs2 = Integer.valueOf(rs2Tampon);
			   myRd = Integer.valueOf(rdTampon);
			   
			   System.out.print("Rs1 vaut:" + myRs1 + " ");
			   System.out.print("Rs2 vaut:" + myRs2 + " ");
			   System.out.println("Rd vaut:" + myRd + " ");
			   System.out.println();

			   
			   a.setOpcode(abc.getOpcode());
			   
			   a.setRs1(myRs1);
			   a.setRs2(myRs2);
			   a.setRd(myRd);  
			   a.setInstrBinaire();
			   
			   int[] tab0 = a.getInstrBinaire();
			   System.out.println("L'instruction en binaire vaut:");
				for (int j = 0; j < 32 ; j++){System.out.print(tab0[j]);}
				System.out.println();
			   
			   a.setInstrHexa();
			   
			   myListeInstruction.addInstruction(a); 
			   System.out.println("La longueur de la liste vaut:" + myListeInstruction.lenght());
			   
			   break;
			   
		   case 1:
			   I b = new I(); 
			   
			   rdTampon = st2.nextToken();
			   rs1Tampon = st2.nextToken();
			   immediateTampon  = st2.nextToken();
			   
			   rdTampon = rdTampon.substring(1, rdTampon.length());
			   rs1Tampon = rs1Tampon.substring(1, rs1Tampon.length());
			   
			   myRs1 = Integer.valueOf(rs1Tampon);
			   myRd = Integer.valueOf(rdTampon);
			   myImmediate = Integer.valueOf(immediateTampon);
			   
			   System.out.print("Rs1 vaut:" + myRs1+ " ");
			   System.out.print("Rd vaut:" + myRd+ " ");
			   System.out.println("Immediate vaut:" + myImmediate+ " ");
			   System.out.println();
			   
			   b.setOpcode(abc.getOpcode());
			   
			   b.setRs1(myRs1);
			   b.setRd(myRd);
			   b.setImmediate(myImmediate); 
			   b.setInstrBinaire();
			   
			   int[] tab1 = b.getInstrBinaire();
			   System.out.println("L'instruction en binaire vaut:");
				for (int j = 0; j < 32 ; j++){System.out.print(tab1[j]);}
				System.out.println();
				
			   b.setInstrHexa();
			   
			   myListeInstruction.addInstruction(b); 
			   System.out.println("La longueur de la liste vaut:" + myListeInstruction.lenght());
			   
			   break;
			   
		   case 2: 			   
			   J c = new J();
			   
			   valueTampon = st2.nextToken();
			   
			   c.setOpcode(abc.getOpcode());
			   
			   if (valueTampon.equalsIgnoreCase("fin")) {
				   c.setValue(valueTampon);
				   int[] fin = {0,0,0,0,0,1};
				   c.setOpcode(fin);
				   System.out.println("c'est la fin du code");
			   }else{
				   myValue = Integer.valueOf(valueTampon);
				   c.setValue(myValue); 
				   System.out.println("c'est un simple jump");
			   }
			   System.out.println("Value vaut:" + myValue + " ");
			   System.out.println();

			   
			   c.setInstrBinaire();
			   
			   int[] tab2 = c.getInstrBinaire();
			   System.out.println("L'instruction en binaire vaut:");
				for (int j = 0; j < 32 ; j++){System.out.print(tab2[j]);}
				System.out.println();
				
			   
			   c.setInstrHexa();
			   
			   myListeInstruction.addInstruction(c);   
			   System.out.println("La longueur de la liste vaut:" + myListeInstruction.lenght());
			   break;
			   
			   default : System.out.println("Le type de la ligne n'a pas pu �tre ientifier � la ligne :" + compteur);
  
		   
		   }

		   System.out.println("===================================================");
		   System.out.println("===================================================");
		   System.out.println("===================================================");
		   
		   } ; 
	   
		lecteurFichierEntre.close(); 
		

		myListeInstruction.editionFichier();
		
		  
		  }
		  
	  }


             
	  }

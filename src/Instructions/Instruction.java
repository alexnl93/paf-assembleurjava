package Instructions;

public class Instruction {


	private int[] Rs1 = new int[5] ;
	private int[] Rs2 = new int[5];
	private int[] Rd = new int[5];
	private int[] Opcode = new int[6];
	private int[] Immediate = new int[16];
	private int[] Value = new int[26];
	protected int[] InstrBinaire = new int[32];
	private int Type = 0;
	private String Inst = new String();
	private String InstrHexa = "" ;
	

	//les deuxi�mes m�thode � appliquer faire un case en fonction du type
	public int[] getRs1() {

		return Rs1;
	}
	
	public boolean setRs1(int rs1) { //est faux si la valeur n'est pas bonne
		
		boolean p = true;
		
		if ( rs1 < 0) {
			p = false;
			System.out.println("La valeur de rs1 est negative"); 
			};
		
		if ( rs1 > 31) {
			p = false;
			System.out.println("La valeur de rs1 est trop grande");
			};
		
		for (int i = 0 ; i <= 4; i++){
			Rs1[4-i] = rs1 & 0x1 ;
			rs1 = rs1 >> 1;
		}
		
		System.out.print("Rs1 en binaire vaut:");
		for (int j = 0; j < 5 ; j++){System.out.print(Rs1[j]);}
		System.out.println();
		System.out.println();
		
		
		return p;
		
	}
	
     public int[] getRs2() {
		return Rs2;
	}
     
	public boolean setRs2(int rs2) {
		
		boolean p = true;
		
		if ( rs2 < 0) {
			p = false;
			System.out.println("La valeur de rs2 est negative"); 
			};
		
		if ( rs2 > 31) {
			p=false;
			System.out.println("La valeur de rs2 est trop grande");
			};
		
		for (int i = 0 ; i <= 4; i++){
			Rs2[4-i] = rs2 & 0x1 ;
			rs2 = rs2 >> 1;
		}
		
		System.out.print("Rs2 en binaire vaut:");
		for (int j = 0; j < 5 ; j++){System.out.print(Rs2[j]);}
		System.out.println();
		System.out.println();
		
		return p;
	}
	
	public int[] getRd() {
		return Rd;
	}
	
	public boolean setRd(int rd) {
		
		boolean p = true;
		
		if ( rd < 0) {
			p=false;
			System.out.println("La valeur de rd est negative");
		}
		
		if ( rd > 31) {
			p=false;
			System.out.println("La valeur de rd est trop grande");
		}
		
		for (int i = 0 ; i <= 4; i++){
			Rd[i] = rd & 0x1 ;
			rd = rd >> 1;
		}
		
		System.out.print("Rd en binaire vaut:");
		for (int j = 0; j < 5 ; j++){System.out.print(Rd[4-j]);}
		System.out.println();
		System.out.println();
		
		return p ;
	}
	
	public int[] getOpcode() {
		return Opcode;
	}

	public void setOpcode(int[] opcode) {
		Opcode = opcode;
	}

	public int[] getImmediate() {
		return Immediate;
	}
	
	public boolean setImmediate(int immediate) {
		
		boolean p = true;
		
		if ( immediate < - 65535 ) 
			{
			p = false;
			System.out.println("La valeur de immediate est trop petite"); 
			};
		
		if ( immediate > 65535) 
			{
			p = false;
			System.out.println("La valeur de immediate est trop grande");
			};
			
			for (int i = 0 ; i <= 15; i++){
				Immediate[15-i] = immediate & 0x1 ;
				immediate = immediate >> 1;
			}
			
			System.out.print("Immediate en binaire vaut:");
			for (int j = 0; j < 16 ; j++){System.out.print(Immediate[j]);}
			System.out.println();
			System.out.println();

		
		return p;
	}
	
	public int[] getValue() {
		return Value;
	}
	
	public boolean setValue(int value) {

		boolean p = true;
		
		if ( value < - 67108864 ) 
			{
			p = false;
			System.out.println("La valeur de immediate est trop petite");
			};
		
		if ( value > 67108864) 
			{
			p = false;
			System.out.println("La valeur de immediate est trop grande");
			};
		
			for (int i = 0 ; i <= 25; i++){
				Value[25-i] = value & 0x1 ;
				value = value >> 1;
			}
			
			System.out.print("Value en binaire vaut:");
			for (int j = 0; j < 26 ; j++){System.out.print(Value[j]);}
			System.out.println();
			System.out.println();
		
		return p;
	}
	
	public boolean setValue(String value){ 
		
		boolean p = true;
		 for (int i = 0 ; i < 26 ; i++){ Value[i] = 0; }
		if (value.equals("fin")) { p = true; } else { p = false; }
		return p; 
	}
	
	//troisi�me m�thode � appliquer
	public void setInstrBinaire() {
	 //overide	
	}
	
	public int[] getInstrBinaire() {
		return InstrBinaire;
	}
	
	public String getInstrHexa() {
		return InstrHexa;
	}
	
	public int getType() {
		return Type;
	}
	
	public String getInst() {
		return Inst;
	}
	
	public void setInst(String inst) {
		Inst = inst;
	}
	
	//quatir�me m�thode � appliquer
	public void setInstrHexa() {
		
		String[] InstrHexa2 = new String[8] ;	
	    String InstrBinaire2 = new String();
	    
	    InstrHexa = ""; 
	    InstrBinaire2 = "";
	    
	    
	    for (int j = 0; j < 32 ; j++) { InstrBinaire2 += Integer.toString(InstrBinaire[j]);}
	    
	    
	       System.out.println("L'instruction en binaire (c'est du string) vaut:");
		   System.out.println( InstrBinaire2);
		   System.out.println();
		   
		   for (int j = 0; j < 8 ; j++){   
		    		
	//		   System.out.println( InstrBinaire2.substring(4*j, 4*j+4));
			   
			   if (InstrBinaire2.substring(4*j, 4*j+4).equals("0000")){InstrHexa2[j]="0";
			   } else if (InstrBinaire2.substring(4*j, 4*j+4).equals("0001")){InstrHexa2[j]="1";
			   } else if (InstrBinaire2.substring(4*j, 4*j+4).equals("0010")){InstrHexa2[j]="2";
			   } else if (InstrBinaire2.substring(4*j, 4*j+4).equals("0011")){InstrHexa2[j]="3";
			   } else if (InstrBinaire2.substring(4*j, 4*j+4).equals("0100")){InstrHexa2[j]="4";
			   } else if (InstrBinaire2.substring(4*j, 4*j+4).equals("0101")){InstrHexa2[j]="5";
			   } else if (InstrBinaire2.substring(4*j, 4*j+4).equals("0110")){InstrHexa2[j]="6";
			   } else if (InstrBinaire2.substring(4*j, 4*j+4).equals("0111")){InstrHexa2[j]="7";
			   } else if (InstrBinaire2.substring(4*j, 4*j+4).equals("1000")){InstrHexa2[j]="8";
			   } else if (InstrBinaire2.substring(4*j, 4*j+4).equals("1001")){InstrHexa2[j]="9";
			   } else if (InstrBinaire2.substring(4*j, 4*j+4).equals("1010")){InstrHexa2[j]="a";
			   } else if (InstrBinaire2.substring(4*j, 4*j+4).equals("1011")){InstrHexa2[j]="b";
			   } else if (InstrBinaire2.substring(4*j, 4*j+4).equals("1100")){InstrHexa2[j]="c";
			   } else if (InstrBinaire2.substring(4*j, 4*j+4).equals("1101")){InstrHexa2[j]="d";
			   } else if (InstrBinaire2.substring(4*j, 4*j+4).equals("1110")){InstrHexa2[j]="e";
			   } else if (InstrBinaire2.substring(4*j, 4*j+4).equals("1111")){InstrHexa2[j]="f";			   
			   } else {System.out.println("Aucun caract�re en hexad�cimal n'a �t� reconu");}
			
			   
//		   System.out.println("Lettre vaut:" + InstrHexa2[j]);    	 
		   InstrHexa += InstrHexa2[j];} 
		   
		   System.out.println("L'instruction en hexad�cimal vaut:" + InstrHexa);
		   
		   
	}
	
	//premi�re m�thode � appliquer
	public void tri(){

		switch(Inst)  //Instr sera le mot scanner de la premi�re ligne
		{ 
		case  "ADD": 
			this.Type = 0;
//			this.Opcode = 0x20;
			this.Opcode[0] = 1; 
			this.Opcode[1] = 0; 
			this.Opcode[2] = 0;
			this.Opcode[3] = 0; 
			this.Opcode[4] = 0; 
			this.Opcode[5] = 0; 
			break;
		case "ADDI": 
			this.Type = 1;
//			this.Opcode = 0x08;
			this.Opcode[0] = 0; 
			this.Opcode[1] = 0; 
			this.Opcode[2] = 1;
			this.Opcode[3] = 0; 
			this.Opcode[4] = 0; 
			this.Opcode[5] = 0;  
			break;
		case "AND": 
			this.Type = 0;
//			this.Opcode = 0x24;
			this.Opcode[0] = 0; 
			this.Opcode[1] = 1; 
			this.Opcode[2] = 1;
			this.Opcode[3] = 0; 
			this.Opcode[4] = 0; 
			this.Opcode[5] = 0;  
			break;
		case "ANDI": 
			this.Type = 1;
//			this.Opcode = 0x0c;
			this.Opcode[0] = 0; 
			this.Opcode[1] = 0; 
			this.Opcode[2] = 1;
			this.Opcode[3] = 1; 
			this.Opcode[4] = 0; 
			this.Opcode[5] = 0;  
			break;
		case "BEQZ": 
			this.Type = 1; 
//			this.Opcode = 0x04;
			this.Opcode[0] = 0; 
			this.Opcode[1] = 0; 
			this.Opcode[2] = 0;
			this.Opcode[3] = 1; 
			this.Opcode[4] = 0; 
			this.Opcode[5] = 0; 
			break;
		case "BNEZ": 
			this.Type = 1;  
//			this.Opcode = 0x05;
			this.Opcode[0] = 0; 
			this.Opcode[1] = 0; 
			this.Opcode[2] = 0;
			this.Opcode[3] = 1; 
			this.Opcode[4] = 0; 
			this.Opcode[5] = 1; 
			break;
		case "J": 
			this.Type = 2; 
//			this.Opcode = 0x02; 
			this.Opcode[0] = 0; 
			this.Opcode[1] = 0; 
			this.Opcode[2] = 0;
			this.Opcode[3] = 0; 
			this.Opcode[4] = 1; 
			this.Opcode[5] = 0; 
			break;
		case "JAL": 
			this.Type = 2; 
//			this.Opcode = 0x03; 
			this.Opcode[0] = 0; 
			this.Opcode[1] = 0; 
			this.Opcode[2] = 0;
			this.Opcode[3] = 0; 
			this.Opcode[4] = 1; 
			this.Opcode[5] = 1; 
			break;
		case "JALR": 
			this.Type = 1; 
//			this.Opcode = 0x13; 
			this.Opcode[0] = 0; 
			this.Opcode[1] = 1; 
			this.Opcode[2] = 0;
			this.Opcode[3] = 0; 
			this.Opcode[4] = 1; 
			this.Opcode[5] = 1; 
			break;
		case "JR": 
			this.Type = 0; 
//			this.Opcode = 0x12; 
			this.Opcode[0] = 0; 
			this.Opcode[1] = 1; 
			this.Opcode[2] = 0;
			this.Opcode[3] = 0; 
			this.Opcode[4] = 1; 
			this.Opcode[5] = 0; 
			break;
		case "LHI": 
			this.Type = 1; 
//			this.Opcode = 0x0f; 
			this.Opcode[0] = 0; 
			this.Opcode[1] = 0; 
			this.Opcode[2] = 1;
			this.Opcode[3] = 1; 
			this.Opcode[4] = 1; 
			this.Opcode[5] = 1; 
			break;
		case "LW": 
			this.Type = 1; 
//			this.Opcode = 0x23; 
			this.Opcode[0] = 1; 
			this.Opcode[1] = 0; 
			this.Opcode[2] = 0;
			this.Opcode[3] = 0; 
			this.Opcode[4] = 1; 
			this.Opcode[5] = 1; 
			break;
		case "OR": 
			this.Type = 0; 
//			this.Opcode = 0x25; 
			this.Opcode[0] = 1; 
			this.Opcode[1] = 0; 
			this.Opcode[2] = 0;
			this.Opcode[3] = 1; 
			this.Opcode[4] = 0; 
			this.Opcode[5] = 1; 
			break;
		case "ORI": 
			this.Type = 1; 
//			this.Opcode = 0x0d; 
			this.Opcode[0] = 0; 
			this.Opcode[1] = 0; 
			this.Opcode[2] = 1;
			this.Opcode[3] = 1; 
			this.Opcode[4] = 0; 
			this.Opcode[5] = 1; 
			break;
		case "SEQ": 
			this.Type = 0; 
//			this.Opcode = 0x28; 
			this.Opcode[0] = 1; 
			this.Opcode[1] = 0; 
			this.Opcode[2] = 1;
			this.Opcode[3] = 0; 
			this.Opcode[4] = 0; 
			this.Opcode[5] = 0; 
			break;
		case "SEQI": 
			this.Type = 1; 
//			this.Opcode = 0x18; 
			this.Opcode[0] = 0; 
			this.Opcode[1] = 1; 
			this.Opcode[2] = 1;
			this.Opcode[3] = 0; 
			this.Opcode[4] = 0; 
			this.Opcode[5] = 0; 
			break;
		case "SLE": 
			this.Type = 0; 
//			this.Opcode = 0x2c; 
			this.Opcode[0] = 1; 
			this.Opcode[1] = 0; 
			this.Opcode[2] = 1;
			this.Opcode[3] = 1; 
			this.Opcode[4] = 0; 
			this.Opcode[5] = 0; 
			break;
		case "SLEI": 
			this.Type = 1; 
//			this.Opcode = 0x1c; 
			this.Opcode[0] = 0; 
			this.Opcode[1] = 1; 
			this.Opcode[2] = 1;
			this.Opcode[3] = 1; 
			this.Opcode[4] = 0; 
			this.Opcode[5] = 0; 
			break;
		case "SLL": 
			this.Type = 0; 
//			this.Opcode = 0x04; 
			this.Opcode[0] = 0; 
			this.Opcode[1] = 0; 
			this.Opcode[2] = 0;
			this.Opcode[3] = 1; 
			this.Opcode[4] = 0; 
			this.Opcode[5] = 0; 
			break;
		case "SLLI": 
			this.Type = 1; 
//			this.Opcode = 0x14; 
			this.Opcode[0] = 0; 
			this.Opcode[1] = 1; 
			this.Opcode[2] = 0;
			this.Opcode[3] = 1; 
			this.Opcode[4] = 0; 
			this.Opcode[5] = 0; 
			break;
		case "SLT": 
			this.Type = 0; 
//			this.Opcode = 0x2a; 
			this.Opcode[0] = 1; 
			this.Opcode[1] = 0; 
			this.Opcode[2] = 1;
			this.Opcode[3] = 0; 
			this.Opcode[4] = 1; 
			this.Opcode[5] = 0; 
			break;
		case "SLTI": 
			this.Type = 1; 
//			this.Opcode = 0x1a; 
			this.Opcode[0] = 0; 
			this.Opcode[1] = 1; 
			this.Opcode[2] = 1;
			this.Opcode[3] = 0; 
			this.Opcode[4] = 1; 
			this.Opcode[5] = 0; 
			break;
		case "SNE": 
			this.Type = 0; 
//			this.Opcode = 0x29; 
			this.Opcode[0] = 1; 
			this.Opcode[1] = 0; 
			this.Opcode[2] = 1;
			this.Opcode[3] = 0; 
			this.Opcode[4] = 0; 
			this.Opcode[5] = 1; 
			break;
		case "SNEI": 
			this.Type = 1; 
//			this.Opcode = 0x19; 
			this.Opcode[0] = 1; 
			this.Opcode[1] = 0; 
			this.Opcode[2] = 1;
			this.Opcode[3] = 0; 
			this.Opcode[4] = 0; 
			this.Opcode[5] = 1; 
			break;
		case "SRA": 
			this.Type = 0; 
//			this.Opcode = 0x07; 
			this.Opcode[0] = 0; 
			this.Opcode[1] = 0; 
			this.Opcode[2] = 0;
			this.Opcode[3] = 1; 
			this.Opcode[4] = 1; 
			this.Opcode[5] = 1; 
			break;
		case "SRAI": 
			this.Type = 1; 
//			this.Opcode = 0x17; 
			this.Opcode[0] = 0; 
			this.Opcode[1] = 1; 
			this.Opcode[2] = 0;
			this.Opcode[3] = 1; 
			this.Opcode[4] = 1; 
			this.Opcode[5] = 1; 
			break;
		case "SRL": 
			this.Type = 0; 
//			this.Opcode = 0x06; 
			this.Opcode[0] = 0; 
			this.Opcode[1] = 0; 
			this.Opcode[2] = 0;
			this.Opcode[3] = 1; 
			this.Opcode[4] = 1; 
			this.Opcode[5] = 0; 
			break;
		case "SRLI": 
			this.Type = 1; 
//			this.Opcode = 0x16; 
			this.Opcode[0] = 0; 
			this.Opcode[1] = 1; 
			this.Opcode[2] = 0;
			this.Opcode[3] = 1; 
			this.Opcode[4] = 1; 
			this.Opcode[5] = 0; 
			break;
		case "SUB": 
			this.Type = 0; 
//			this.Opcode = 0x22; 
			this.Opcode[0] = 1; 
			this.Opcode[1] = 0; 
			this.Opcode[2] = 0;
			this.Opcode[3] = 0; 
			this.Opcode[4] = 1; 
			this.Opcode[5] = 0; 
			break;
		case "SUBI": 
			this.Type = 1; 
//			this.Opcode = 0x0a; 
			this.Opcode[0] = 0; 
			this.Opcode[1] = 0; 
			this.Opcode[2] = 1;
			this.Opcode[3] = 0; 
			this.Opcode[4] = 1; 
			this.Opcode[5] = 0; 
			break;
		case "SW": 
			this.Type = 1; 
//			this.Opcode = 0x2b; 
			this.Opcode[0] = 1; 
			this.Opcode[1] = 0; 
			this.Opcode[2] = 1;
			this.Opcode[3] = 0; 
			this.Opcode[4] = 1; 
			this.Opcode[5] = 1; 
			break;
		case "XOR": 
			this.Type = 0; 
//			this.Opcode = 0x26; 
			this.Opcode[0] = 1; 
			this.Opcode[1] = 0; 
			this.Opcode[2] = 0;
			this.Opcode[3] = 1; 
			this.Opcode[4] = 1; 
			this.Opcode[5] = 0; 
			break;
		case "XORI": 
			this.Type = 1; 
//			this.Opcode = 0x0e; *
			this.Opcode[0] = 0; 
			this.Opcode[1] = 0; 
			this.Opcode[2] = 1;
			this.Opcode[3] = 1; 
			this.Opcode[4] = 1; 
			this.Opcode[5] = 0; 
			break;
		default: System.out.println("Attention Exception:");
				 System.out.println("Le code commande " + Inst + "n'est pas un code valide" );
		  }
		 }

}
